const mongoose = require("mongoose");
const config = require("./config");
const { nanoid } = require("nanoid");
const User = require("./models/User");
const Place = require("./models/Place");
const Gallery = require("./models/Gallery");

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user, admin] = await User.create(
    {
      email: "user@gmail.com",
      password: "12345",
      token: nanoid(),
      role: "user",
      displayName: "User",
      avatar: "fixtures/user.png",
    },
    {
      email: "admin@gmail.com",
      password: "12345",
      token: nanoid(),
      role: "admin",
      displayName: "Admin",
      avatar: "fixtures/admin.png",
    }
  );

  const [place13, place2, place3, place4] = await Place.create(
    {
      id: nanoid(),
      user: user,
      title: "Adrianno13",
      description:
        "Coffee is a brewed drink prepared from roasted coffee beans, the seeds of berries from certain Coffea species. ... Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans, primarily due to its caffeine content",
      image: "fixtures/cafe_1.jpeg",
    },
    {
      id: nanoid(),
      user: admin,
      title: "Adrianno2",
      description:
        "Coffee is a brewed drink prepared from roasted coffee beans, the seeds of berries from certain Coffea species. ... Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans, primarily due to its caffeine content. ",
      image: "fixtures/cafe_2.jpeg",
    },
    {
      id: nanoid(),
      user: user,
      title: "Adrianno1",
      description:
        "Coffee is a brewed drink prepared from roasted coffee beans, the seeds of berries from certain Coffea species. ... Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans, primarily due to its caffeine content",
      image: "fixtures/cafe_1.jpeg",
    },
    {
      id: nanoid(),
      user: admin,
      title: "Adrianno2",
      description:
        "Coffee is a brewed drink prepared from roasted coffee beans, the seeds of berries from certain Coffea species. ... Coffee is darkly colored, bitter, slightly acidic and has a stimulating effect in humans, primarily due to its caffeine content. ",
      image: "fixtures/cafe_2.jpeg",
    }
  );

  await Gallery.create(
    {
      place: place13,
      image: "fixtures/gallery1.jpeg",
      user,
    },
    {
      place: place13,
      image: "fixtures/gallery2.jpeg",
      user,
    },
    {
      place: place13,
      image: "fixtures/gallery3.jpeg",
      user,
    },
    {
      place: place13,
      image: "fixtures/gallery4.jpeg",
      user,
    },
    {
      place: place13,
      image: "fixtures/gallery1.jpeg",
      user,
    },
    {
      place: place2,
      image: "fixtures/gallery2.jpeg",
      user,
    },
    {
      place: place2,
      image: "fixtures/gallery3.jpeg",
      user,
    },
    {
      place: place2,
      image: "fixtures/gallery4.jpeg",
      user,
    },
    {
      place: place3,
      image: "fixtures/gallery1.jpeg",
      user,
    },
    {
      place: place3,
      image: "fixtures/gallery2.jpeg",
      user,
    },
    {
      place: place3,
      image: "fixtures/gallery3.jpeg",
      user,
    },
    {
      place: place3,
      image: "fixtures/gallery4.jpeg",
      user,
    },
    {
      place: place4,
      image: "fixtures/gallery1.jpeg",
      user,
    },
    {
      place: place4,
      image: "fixtures/gallery2.jpeg",
      user,
    },
    {
      place: place4,
      image: "fixtures/gallery3.jpeg",
      user,
    },
    {
      place: place4,
      image: "fixtures/gallery4.jpeg",
      user,
    }
  );

  await mongoose.connection.close();
};

run().catch((e) => {
  console.error(e);
  process.exit(1);
});
