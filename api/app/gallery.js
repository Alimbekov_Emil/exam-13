const express = require("express");
const Gallery = require("../models/Gallery");
const upload = require("../multer").gallery;
const auth = require("../middleware/auth.js");
const permit = require("../middleware/permit");

const router = express.Router();

router.get("/:id", async (req, res) => {
  try {
    const gallery = await Gallery.find({ place: req.params.id });
    return res.send(gallery);
  } catch (e) {
    return res.sendStatus(400);
  }
});

router.post("/", auth, upload.single("image"), async (req, res) => {
  try {
    const gallery = await new Gallery({
      user: req.user,
      place: req.body.place,
      image: req.file ? req.file.filename : null,
    });
    await gallery.save();
    return res.send(gallery);
  } catch (e) {
    return res.sendStatus(400);
  }
});

router.delete("/:id", [auth, permit("admin")], async (req, res) => {
  try {
    await Gallery.deleteOne({ _id: req.params.id });
    return res.send("Your Gallery deleted");
  } catch (e) {
    return res.sendStatus(400);
  }
});

module.exports = router;
