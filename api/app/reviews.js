const express = require("express");
const auth = require("../middleware/auth");
const router = express.Router();
const Review = require("../models/Review");
const permit = require("../middleware/permit");


router.get("/:id", async (req, res) => {
  try {
    const reviews = await Review.find({ place: req.params.id })
      .populate("user", "_id, displayName")
      .populate("place", "_id, title")
      .sort({ datetime: -1 });
    return res.send(reviews);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.delete("/:id", [auth, permit("admin")], async (req, res) => {
  try {
    await Review.deleteOne({ _id: req.params.id });
    return res.send("Deleted Review");
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post("/", auth, async (req, res) => {
  try {
    const review = await new Review({
      user: req.user,
      place: req.body.place,
      text: req.body.text,
      datetime: new Date().toISOString(),
      qualityOfFood: req.body.qualityOfFood,
      serviceQuality: req.body.serviceQuality,
      interior: req.body.interior,
    });
    await review.save();
    return res.send(review);
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;
