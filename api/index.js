const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const exitHook = require("async-exit-hook");
const config = require("./config");
const users = require("./app/users");
const places = require("./app/places");
const reviews = require("./app/reviews");
const gallery = require("./app/gallery");

const app = express();
app.use(cors());
app.use(express.static("public"));
app.use(express.json());

const port = 8000;

app.use("/users", users);
app.use("/places", places);
app.use("/reviews", reviews);
app.use("/gallery", gallery);

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });

  exitHook(async (callback) => {
    await mongoose.disconnect();
    console.log("mongoose disconnected");
    callback();
  });
};

run().catch(console.error);
