const { I } = inject();

Given("я нахожусь на странице логина", () => {
  I.amOnPage("/login");
});

When("я ввожу в поля текст:", (table) => {
  for (const id in table.rows) {
    if (id < 1) {
      continue;
    }

    const cells = table.rows[id].cells;
    const field = cells[0].value;
    const value = cells[1].value;

    I.fillField(field, value);
  }
});

When("нажимаю на кнопку {string}", (name) => {
  I.click(`//button//*[contains(text(),"${name}")]/..`);
});

When("вижу {string}", (str) => {
  I.see(str);
});

Given("я нахожусь на старнице добавления заведения", () => {
  I.amOnPage("/places/new");
});

When("я ввожу {string} в поле {string}", (table) => {
  for (const id in table.rows) {
    if (id < 1) {
      continue;
    }

    const cells = table.rows[id].cells;
    const field = cells[0].value;
    const value = cells[1].value;

    I.fillField(field, value);
  }
});

When("я загружаю картинку", (name) => {
  I.attachFile({ xpath: "//input[@id='image']" }, "image/admin.png");
});

When("я соглашаюсь с условиями", () => {
  I.click("input[type=checkbox]");
});

When("я нажимаю кнопку для отправки {string}", (name) => {
  I.click("Submit New Places");
});

Then("я вижу заголовок заведения {string}", (str) => {
  I.see(str);
});

Given("я нахожусь на старнице главной старнице", () => {
  I.amOnPage("/");
});

When("я нажимаю на заголовок {string}", (str) => {
  I.click(str);
});

When("я ввожу {string} в поле {string}", (table) => {
  for (const id in table.rows) {
    if (id < 1) {
      continue;
    }

    const cells = table.rows[id].cells;
    const field = cells[0].value;
    const value = cells[1].value;

    I.fillField(field, value);
  }
});

Then("выбираю рейтинг в звездочках", () => {
  I.click("span[5 Stars]");
  I.wait(5);
});

Then("рейтинг изменится", () => {
  // From "features/AddNewPlaces.feature" {"line":39,"column":5}
  throw new Error("Not implemented yet");
});
