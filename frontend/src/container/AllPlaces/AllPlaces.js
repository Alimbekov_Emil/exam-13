import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { fetchPlacesRequest } from "../../store/actions/placesActions";
import Place from "./Place";
import { Grid } from "@material-ui/core";

const AllPlaces = () => {
  const dispatch = useDispatch();
  const places = useSelector((state) => state.places.places);

  useEffect(() => {
    dispatch(fetchPlacesRequest());
  }, [dispatch]);

  return (
    <Grid container spacing={2}>
      {places.map((place) => (
        <Grid item key={place._id} style={{ width: "24%" }}>
          <Place title={place.title} id={place._id} image={place.image} />
        </Grid>
      ))}
    </Grid>
  );
};

export default AllPlaces;
