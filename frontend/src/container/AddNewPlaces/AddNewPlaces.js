import React, { useState } from "react";
import { Grid } from "@material-ui/core";
import { Typography, Button } from "@material-ui/core";
import FormElement from "../../components/UI/Form/FormElement";
import FileInput from "../../components/UI/Form/FileInput";
import Checkbox from "@material-ui/core/Checkbox";
import { useDispatch } from "react-redux";
import { createPlaceRequest } from "../../store/actions/placesActions";

const AddNewPlaces = () => {
  const dispatch = useDispatch();

  const [place, setPlace] = useState({
    title: "",
    description: "",
    image: "",
    checked: false,
  });

  const inputChangeHandler = (e) => {
    const { name, value } = e.target;

    setPlace((prev) => ({ ...prev, [name]: value }));
  };

  const fileChangeHandler = (e) => {
    const name = e.target.name;
    const file = e.target.files[0];

    setPlace((prevState) => ({
      ...prevState,
      [name]: file,
    }));
  };

  const submitFormHandler = (e) => {
    e.preventDefault();

    const formData = new FormData();

    Object.keys(place).forEach((key) => {
      formData.append(key, place[key]);
    });

    dispatch(createPlaceRequest(formData));
  };

  return (
    <Grid container spacing={2} direction="column" component={"form"} onSubmit={submitFormHandler}>
      <Typography variant="h5">Add new places</Typography>
      <Grid item>
        <FormElement label="Title" name="title" value={place.title} onChange={inputChangeHandler} />
      </Grid>
      <Grid item>
        <FormElement
          label="Description"
          name="description"
          value={place.description}
          onChange={inputChangeHandler}
        />
      </Grid>
      <Grid item>
        <FileInput label="Image" name="image" onChange={fileChangeHandler} />
      </Grid>
      <Grid item container direction="row" wrap="nowrap" spacing={2}>
        <Grid item style={{ width: "50%" }}>
          <Typography variant="h6">
            By submitting this form, you agree that the following information will be submitted to the public
            domain, and administrators of the site will have full control over the said information
          </Typography>
        </Grid>
        <Grid item>
          <Checkbox
            checked={place.checked}
            onChange={(e) => setPlace((prev) => ({ ...prev, checked: e.target.checked }))}
          />
        </Grid>
      </Grid>
      <Grid item>
        <Button variant="contained" color="primary" type="submit">
          Submit New Places
        </Button>
      </Grid>
    </Grid>
  );
};

export default AddNewPlaces;
