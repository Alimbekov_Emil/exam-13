import { put, takeEvery } from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {
  createGalleryFailure,
  createGalleryRequest,
  createGallerySuccess,
  deleteGalleryFailure,
  deleteGalleryRequest,
  deleteGallerySuccess,
  fetchGalleryFailure,
  fetchGalleryRequest,
  fetchGallerySuccess,
} from "../actions/galleryActions";

export function* createGallery({ payload: galleryData }) {
  try {
    const response = yield axiosApi.post("/gallery", galleryData.formData);
    yield put(createGallerySuccess(response.data));
    yield put(fetchGalleryRequest(galleryData.place));
  } catch (error) {
    yield put(createGalleryFailure(error.response.data));
  }
}

export function* fetchGallery({ payload: placeId }) {
  try {
    const response = yield axiosApi.get("/gallery/" + placeId);
    yield put(fetchGallerySuccess(response.data));
  } catch (error) {
    yield put(fetchGalleryFailure(error.response.data));
  }
}

export function* deleteGallery({ payload: id }) {
  try {
    const response = yield axiosApi.delete("/gallery/" + id.galleryId);
    yield put(deleteGallerySuccess(response.data));
    yield put(fetchGalleryRequest(id.placeId));
  } catch (error) {
    yield put(deleteGalleryFailure(error.response.data));
  }
}

const gallerySagas = [
  takeEvery(createGalleryRequest, createGallery),
  takeEvery(fetchGalleryRequest, fetchGallery),
  takeEvery(deleteGalleryRequest, deleteGallery),
];

export default gallerySagas;
