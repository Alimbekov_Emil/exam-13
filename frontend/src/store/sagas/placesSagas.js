import { put, takeEvery } from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import { historyPush } from "../actions/historyActions";
import { addNotification } from "../actions/notifierActions";
import {
  createPlaceRequest,
  createPlaceSuccess,
  createPlaceFailure,
  fetchPlacesFailure,
  fetchPlacesRequest,
  fetchPlacesSuccess,
  fetchSoloPlaceSuccess,
  fetchSoloPlaceFailure,
  fetchSoloPlaceRequest,
  deletePlaceSuccess,
  deletePlaceFailure,
  deletePlaceRequest,
} from "../actions/placesActions.js";

export function* createPlace({ payload: placeData }) {
  try {
    const response = yield axiosApi.post("/places", placeData);
    yield put(createPlaceSuccess(response.data));
    yield put(historyPush("/"));
    yield put(addNotification({ message: "You Create Place", options: { variant: "success" } }));
  } catch (error) {
    yield put(createPlaceFailure(error.response.data));
    yield put(
      addNotification({
        message: "Проверьте поставили ли Вы галочку на соглашения",
        options: { variant: "error" },
      })
    );
  }
}

export function* fetchPlaces() {
  try {
    const response = yield axiosApi.get("/places");
    yield put(fetchPlacesSuccess(response.data));
  } catch (error) {
    yield put(fetchPlacesFailure(error.response.data));
  }
}

export function* fetchSoloPlace({ payload: placeId }) {
  try {
    const response = yield axiosApi.get("/places/" + placeId);
    yield put(fetchSoloPlaceSuccess(response.data));
  } catch (error) {
    yield put(fetchSoloPlaceFailure(error.response.data));
  }
}

export function* deletePlace({ payload: placeId }) {
  try {
    const response = yield axiosApi.delete("/places/" + placeId);
    yield put(deletePlaceSuccess(response.data));
    yield put(fetchPlacesRequest());
  } catch (error) {
    yield put(deletePlaceFailure(error.response.data));
  }
}

const placesSagas = [
  takeEvery(createPlaceRequest, createPlace),
  takeEvery(fetchPlacesRequest, fetchPlaces),
  takeEvery(fetchSoloPlaceRequest, fetchSoloPlace),
  takeEvery(deletePlaceRequest, deletePlace),
];

export default placesSagas;
