import { put, takeEvery } from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import { addNotification } from "../actions/notifierActions";
import {
  createReviewsFailure,
  createReviewsRequest,
  createReviewsSuccess,
  deleteReviewFailure,
  deleteReviewRequest,
  deleteReviewSuccess,
  fetchReviewsFailure,
  fetchReviewsRequest,
  fetchReviewsSuccess,
} from "../actions/reviewsActions";

export function* createReviews({ payload: reviewData }) {
  try {
    const response = yield axiosApi.post("/reviews", reviewData);
    yield put(createReviewsSuccess(response.data));
    yield put(addNotification({ message: "Ваш отзыв принят Спасибо", options: { variant: "success" } }));
    yield put(fetchReviewsRequest(reviewData.place));
  } catch (error) {
    yield put(createReviewsFailure(error.response.data));
  }
}

export function* fetchReviews({ payload: id }) {
  try {
    const response = yield axiosApi.get("/reviews/" + id);
    yield put(fetchReviewsSuccess(response.data));
  } catch (error) {
    yield put(fetchReviewsFailure(error.response.data));
  }
}

export function* deleteReview({ payload: id }) {
  try {
    const response = yield axiosApi.delete("/reviews/" + id.reviewId);
    yield put(deleteReviewSuccess(response.data));
    yield put(fetchReviewsRequest(id.placeId));
  } catch (error) {
    yield put(deleteReviewFailure(error.response.data));
  }
}

const reviewSagas = [
  takeEvery(createReviewsRequest, createReviews),
  takeEvery(fetchReviewsRequest, fetchReviews),
  takeEvery(deleteReviewRequest, deleteReview),
];

export default reviewSagas;
