import React from "react";
import MeRating from "../MeRating/MeRating";
import { Grid, Typography } from "@material-ui/core";

const AverageRating = ({ reviews }) => {
  let allQuality = 0;
  let allService = 0;
  let allInterior = 0;

  if (reviews) {
    reviews.forEach((review) => {
      allQuality += review.qualityOfFood / reviews.length;
      allService += review.serviceQuality / reviews.length;
      allInterior += review.interior / reviews.length;
    });
  }
  const averageRating = Math.ceil((allQuality + allService + allInterior) / 3);

  return (
    <Grid item container spacing={2} direction="column" alignItems="center">
      <Grid item container spacing={2} justifyContent="flex-start">
        <Grid item style={{ width: "20%" }}>
          <Typography>OverAll:</Typography>
        </Grid>
        <Grid item style={{ width: "15%" }}>
          <MeRating value={averageRating} readOnly />
        </Grid>
        <Grid item>
          <Typography>{averageRating}</Typography>
        </Grid>
      </Grid>
      <Grid item container spacing={2} justifyContent="flex-start">
        <Grid item style={{ width: "20%" }}>
          <Typography>Quality Of Food</Typography>
        </Grid>
        <Grid item style={{ width: "15%" }}>
          <MeRating value={allQuality} readOnly />
        </Grid>
        <Grid item>
          <Typography>{allQuality}</Typography>
        </Grid>
      </Grid>
      <Grid item container spacing={2} justifyContent="flex-start">
        <Grid item style={{ width: "20%" }}>
          <Typography>Service Quality :</Typography>
        </Grid>
        <Grid item style={{ width: "15%" }}>
          <MeRating value={allService} readOnly />
        </Grid>
        <Grid item>
          <Typography>{allService}</Typography>
        </Grid>
      </Grid>
      <Grid item container spacing={2} justifyContent="flex-start">
        <Grid item style={{ width: "20%" }}>
          <Typography>Interior</Typography>
        </Grid>
        <Grid item style={{ width: "15%" }}>
          <MeRating value={allInterior} readOnly />
        </Grid>
        <Grid item>
          <Typography>{allInterior}</Typography>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default AverageRating;
